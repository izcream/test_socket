const path = require('path');
const webpack = require('webpack')

module.exports = {
    entry: {
        build: path.resolve(__dirname, 'src/socket.js')
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'build.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules|vue\/dist/,
                query: {
                    'presets': ['es2015'],
                    'comments': false
                }
            },

        ]
    },
    babel: {
        presets: ['es2015']
    },
    resolve: {
        root: path.resolve(__dirname, 'node_modules'),
        extensions: ['','.js']
    },
    resolveLoader: {
        root: path.join(__dirname, 'node_modules')
    }

}
