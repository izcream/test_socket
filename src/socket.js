import io from 'socket.io-client'
import $ from 'jquery'

const URL = 'https://tchat-ms.tarad.com:443'

const socket = io(URL)

let userData = {
    username: 'natakorn@wewillapp.com',
    _id: '57e38f02c2cd7e3a9a120fd1'
}
socket.emit('join', userData)
socket.on('user_connected', (data) => {
    let li = `<li>username: ${data.username}</li>`
    $('#connected').append(li)
})
$('#msg').on('change', (e) => {
    let message = $('#msg').val()
    let sendTo = $('#to').val()
    //จัดเรียงข้อมูลที่จะใช้ในการส่ง
    let sendData = {
        msgData: {
            room_id: '57e38f0fc2cd7e3aa751dcb1',
            channel: 'tarad',
            data_type: 'msg',
            sender_name: userData.username,
            to: sendTo,
            content: message
        },
        roomData: {
            _id: '57e38f0fc2cd7e3aa751dcb1',
            channel: 'tarad',
            type: 'chat',
            isLeaved: false
        }
    }
    //emit ข้อความแชท
    socket.emit('send_chat', sendData)
})
socket.on('received_chat', (data) => {
    let msg = data.msgData
    let li = `<li>Sender: <span class='senderName'>${msg.sender_name}</span>, message: <span class='message'>${msg.content}</span> </li>`
    $('#messages').append(li)
})
